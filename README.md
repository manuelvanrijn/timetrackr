# TimeTrackr

Simple time registration app writen with HAML, SASS and AngularJS (CoffeeScript).

## Setup/Run

```
git clone git@bitbucket.org:manuelvanrijn/timetrackr.git
cd timetrackr
bundle install
rackup
open http://localhost:9292/
```
