#
# Global accessor for the timeTracker module
#
@timeTrackr = @timeTrackr || angular.module('timeTrackr', []);

#
# Find a resource by it's id from a collection
#
@findById = (collection, id) ->
  for resource in collection
    return resource if resource.id == parseInt(id)

#
# Replace a resource with the newResource in a collection
#
@replaceResource = (collection, newResource, extend=false) ->
  resource = findById collection, newResource.id
  if extend
    angular.extend(resource, newResource)
  else
    resource = newResource

#
# Generate some random id
#
@getRandomId = ->
  Math.round Math.random() * 1234
