@timeTrackr.controller "ProjectCtrl", ["$scope", "dataStore", ($scope, dataStore) ->
  $scope.project = {}
  $scope.projects = dataStore.projects

  $scope.editProject = (project) ->
    $scope.project = angular.copy project

  $scope.updateProject = ->
    if $scope.projectForm.$valid
      replaceResource($scope.projects, $scope.project, true)
      dataStore.updateProjects $scope.projects
      $scope.project = {}

  $scope.addProject = ->
    if $scope.projectForm.$valid
      $scope.project.id = getRandomId()
      $scope.projects.push $scope.project
      dataStore.updateProjects $scope.projects
      $scope.project = {}

  $scope.deleteProject = (project) ->
    if confirm 'Are sure you wish to delete this project?'
      project.active = false
      dataStore.updateProjects $scope.projects

  $scope.undeleteProject = (project) ->
    project.active = true
    dataStore.updateProjects $scope.projects
]
