@timeTrackr.controller "RegistrationCtrl", ["$scope", "$filter", "dataStore", ($scope, $filter, dataStore) ->
  $scope.registration = {}
  $scope.registrations = dataStore.registrations
  $scope.projects = dataStore.projects

  $scope.totals = ->
    totals = { earned: 0, hours: 0 }
    for project in $scope.projects
      rate = project.rate
      registrations = $filter('filter')($scope.registrations, {projectId: project.id})
      for registration in registrations
        totals.hours += registration.hours
        totals.earned += rate * registration.hours
    totals

  $scope.getProject = (projectId) ->
    findById $scope.projects, projectId

  $scope.addRegistration = ->
    if $scope.registrationForm.$valid
      $scope.registration.id = getRandomId()
      $scope.registrations.push $scope.registration
      $scope.registration = {}
      $scope.registrationForm.$setPristine()

  $scope.editRegistration = (registration) ->
    $scope.registration = angular.copy registration

  $scope.updateRegistration = ->
    if $scope.registrationForm.$valid
      replaceResource($scope.registrations, $scope.registration, true)
      $scope.registration = {}

  $scope.deleteRegistration = (registration) ->
    if confirm 'Are sure you wish to delete this registration?'
      $scope.registrations.splice $scope.registrations.indexOf(registration), 1

  $scope.$on "projectsChanged", ->
    $scope.projects = dataStore.projects
]
