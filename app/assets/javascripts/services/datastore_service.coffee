@timeTrackr.factory "dataStore", ["$rootScope", ($rootScope) ->
  store = {}
  store.projects = [
    { id: 1, active: true, name: 'TimeTrackr', rate: 50.00 },
    { id: 2, active: true, name: 'Other project', rate: 120.00 }
  ]
  store.registrations = [
    { id: 1, hours: 5, projectId: 1 }
    { id: 2, hours: 1, projectId: 1 }
    { id: 3, hours: 5, projectId: 2 }
    { id: 4, hours: 2, projectId: 1 }
    { id: 5, hours: 1, projectId: 1 }
    { id: 6, hours: 3, projectId: 2 }
    { id: 7, hours: 1, projectId: 1 }
  ]

  store.updateProjects = (projects) ->
    this.projects = projects
    $rootScope.$broadcast('projectsChanged')

  store
]
