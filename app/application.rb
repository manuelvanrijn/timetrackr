module TimeTrackr
  class Application < Sinatra::Base
    set :public_folder, File.expand_path('..', __FILE__)
    set :views, File.expand_path('../views/', __FILE__)

    set :static, true
    set :haml, { :format => :html5 }

    configure :development do
      register Sinatra::Reloader
    end

    # assets
    register Sinatra::AssetSnack
    asset_map '/javascripts/application.js', [File.expand_path('../assets/javascripts/**/*.coffee', __FILE__)]
    asset_map '/stylesheets/application.css', [File.expand_path('../assets/stylesheets/application.scss', __FILE__)]

    get '/' do
      haml :index
    end
  end
end
